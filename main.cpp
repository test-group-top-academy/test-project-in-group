#include <iostream>
using namespace std;

// https://gitlab.com
// https://github.com

int main() {

    return 0;
}

/*
 * Краткий гайд по публикации исходного кода
 * проекта на https://gitlab.com/
 *
 * 1. Регистрируемся на сайте
 * 2. Создаем группу для проектов (необязательный шаг,
 * нужен для более удобной систематизации проектов)
 * 3. Инициализируем систему git в локальной папкой
 * с проектом "git init
 * 4. Добавляем ссылку на удаленный репозиторий
 * git remote add origin https://gitlab.com/test-group-top-academy/test-project-in-group.git
 * 5. Добавляем нужные файлы в репозиторий локально
 * "git add ."
 * 6. Добавляем изменения в историю
 * git commit -m "первые изменения"
 *7. отправляем изменения в удаленный репозиторий
 * git push опционально[--set-upstream origin master]
 * */